# MRH's Android Portfolios

## Certificates

![Cert1](https://bitbucket.org/MuhammadRizky/android_portfolios/raw/ea09b55176920564712dcb5863f27e1b6da8b6e2/image-cert-kotlin.png)

## Football Schedule App

Features : Kotlin, Anko, MVP, SQLite, https://www.thesportsdb.com/ API

![FootballMatchApp1](https://bitbucket.org/MuhammadRizky/android_portfolios/raw/ea09b55176920564712dcb5863f27e1b6da8b6e2/images_FootballMatchApp/football1.png)

![FootballMatchApp2](https://bitbucket.org/MuhammadRizky/android_portfolios/raw/ea09b55176920564712dcb5863f27e1b6da8b6e2/images_FootballMatchApp/football2.png)

![FootballMatchApp3](https://bitbucket.org/MuhammadRizky/android_portfolios/raw/ea09b55176920564712dcb5863f27e1b6da8b6e2/images_FootballMatchApp/football3.png)

![FootballMatchApp4](https://bitbucket.org/MuhammadRizky/android_portfolios/raw/ea09b55176920564712dcb5863f27e1b6da8b6e2/images_FootballMatchApp/football4.png)


## English Studio App

Features : Firebase SignIn, Firebase Database, Firebase Storage, Firebase Cloud Functions

### Screenshot

![EnglishApp1](https://bitbucket.org/MuhammadRizky/android_portfolios/raw/ea09b55176920564712dcb5863f27e1b6da8b6e2/images_EnglishStudioApp/englishapp1.png)

![EnglishApp2](https://bitbucket.org/MuhammadRizky/android_portfolios/raw/ea09b55176920564712dcb5863f27e1b6da8b6e2/images_EnglishStudioApp/englishapp2.png)

![EnglishApp3](https://bitbucket.org/MuhammadRizky/android_portfolios/raw/ea09b55176920564712dcb5863f27e1b6da8b6e2/images_EnglishStudioApp/englishapp3.png)

Thanks!!